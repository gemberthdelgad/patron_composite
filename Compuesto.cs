﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Composite
{
    class Compuesto<T> : IComponente<T>
    {

        //lista donde guardamos los elementos que va a guardar 
        List<IComponente<T>> elementos;
        //Identificador
        public T Nombre { get ; set ; }
        public Compuesto(T pNombre)
        {
            Nombre = pNombre;
            //Instanciamos la lista
            elementos = new List<IComponente<T>>();
        }

        public void Adicionar(IComponente<T> pElemanto)
        {
            elementos.Add(pElemanto);
            
        }

        public IComponente<T> Borrar(T pElemento)
        {
            //buscamos el elemento a borrar
            IComponente<T> elemento = this.Buscar(pElemento);
            //si la encomtramos la eliminamos de la lista
            if(elemento!=null)
            { (this as Compuesto<T>).elementos.Remove(elemento); }
            return this;

        }

        public IComponente<T> Buscar(T pElemento)
        {
            //si es quien se busca se regresa, pElement es el identificador del elemento que qeremos buscar
            if (Nombre.Equals(pElemento))
                return this;

            IComponente<T> encontrado = null;
            //recoremos la lista y buscamos en nuestros elementos
            foreach (IComponente<T> elemento in elementos)
            {
                encontrado = elemento.Buscar(pElemento);
                // este if y brek es para optimicasion
                if (encontrado != null)
                    break;
            }
            return encontrado;
        }

        public string Mostrar(int pProfundidad)
        {
            //Construimos las cadena con una camtidad de - igual a la profundidad 
            StringBuilder infoElemento = new StringBuilder(new String('-', pProfundidad));
            //adicionamos la profundidad del compuesto
            infoElemento.Append("Compuesto" + Nombre + "elementos" + elementos.Count + "\n\r");
            //Adicionamos los elementos
            foreach (IComponente<T> elemento in elementos)
                infoElemento.Append(elemento.Mostrar(pProfundidad + 1));
            return infoElemento.ToString();

        }
    }
}
