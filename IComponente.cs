﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Composite
{

    //Creamos una interfas de tipo generica
    interface IComponente<T>
    {
        //representa a los dato que se guarden en aquel elemento es de tipo "T"
        T Nombre { get; set; }
        // estos metodos son los comportamientos comunes tamto de los compuestos como de los componentes
        // adicionar nos va ha servir para adisionar nuevos elementos siempre y cuando lleven la implementacion de IComponente
        void Adicionar(IComponente<T> pElemanto);
        IComponente<T> Borrar(T pElemento);
        IComponente<T> Buscar(T pElemento);
        //Mostrar nos va a indicar la trasversa del arbol que nosotros tenemos, osea la gerarquia del patron compuesto
        // el valor profundidad tiene que ver simplemente con la profundidad que estemos dentro de la gerarquia 
        string Mostrar(int pProfundidad);
    }
}
