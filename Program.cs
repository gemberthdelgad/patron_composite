﻿using System;

namespace Patron_Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            // parte del cliente
            //como hemos estado trabajando con string colocamos en ambas string ademas "T"es tipo string

            //aqui se instancia un compuesto, este va hacer el elemento principal de la gerarquia,gracias al patron compuesto
            IComponente<string> arbol = new Compuesto<string>("root");// root es identificador
            // trabajo va hacer una variable de referencia, que referencia hacia el nodo
            IComponente<string> trabajo = arbol;//se referencia a erbol para que estee referciado a la raiz

            string opcion="";
            string dato = "";
            // en este ciclo se presenta un menu para crear las jerarquia 
            while (opcion!="6")
            {
                Console.WriteLine("Estoy en {0}",trabajo.Nombre);// esto es importante para que nosotros sepamos donde estamos trabajando y podamos colocar bien los datos
                Console.WriteLine("1.-Adicionar Compuesto 2.-Adicionar Componente 3.-borrar 4.-Encontrar 5.-Mostrar 6.-Salir");
                opcion = Console.ReadLine();
                Console.WriteLine("-----------------");

                if (opcion=="1")
                {
                    // pedimo el nombre del compuesto
                    Console.WriteLine("Dame el nombre del compuesto");
                    dato = Console.ReadLine();
                    IComponente<string> c = new Compuesto<string>(dato);
                    trabajo.Adicionar(c);
                    trabajo = c;

                }
                if (opcion == "2")
                {
                    Console.WriteLine("Dame el nombre del componente:");
                    dato = Console.ReadLine();
                    trabajo.Adicionar(new Componente<string>(dato));

                }
                if (opcion == "3")
                {
                    //solo se podra borrar elementos del compuesto con el que se estee trabajando en ese monento
                    // si desea borrar elementos de otro compuesto tiene que moverse al otro compuesto
                    Console.WriteLine("Dame el elemento a borrar:");
                    dato = Console.ReadLine();
                    trabajo = trabajo.Borrar(dato);
                }
                if (opcion == "4")
                {
                    
                    Console.WriteLine("Dame el elemento a encontrar:");
                    dato = Console.ReadLine();
                    // en este caso  trabajamos desde arbol, porque la busqueda va a empezar desde la raiz
                    trabajo = arbol.Buscar(dato);
                }
                if (opcion == "5")
                {

                    Console.WriteLine(arbol.Mostrar(0));//se pone cero porque vamos trabajando desde la raiz que es 0

                }

            }

        }
    }
}
