﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Composite
{
    //Componente<T> es una clase generica
    class Componente<T> : IComponente<T>
    {
        //Se utiliza como un identificador del componente
        public T Nombre { get; set; }
        // el c resive la variable pnombre y la asigna a la variable interna nombre
        public Componente(T pNombre)
        {
            Nombre = pNombre;
        }
        //como estamos en un componente no puede guardar a otros componentes, entonses  no se adicina 
        //y simple mente se envia un mensage
        public void Adicionar(IComponente<T> pElemanto)
        {
            Console.WriteLine("Solo se adiciona a los compuestos, no a los componentes");
        }
        //al ser un componente pasa lo mismo que adicionar se manda un mensage 
        public IComponente<T> Borrar(T pElemento)
        {
            Console.WriteLine("No se puede eliminar directamente");
            return this;
        }
        // se verifica si el elemento es el mismo que temgamos en el interior
        public IComponente<T> Buscar(T pElemento)
        {
            if (pElemento.Equals(Nombre))
                return this;
            else
                return null;

        }

        public string Mostrar(int pProfundidad)
        {
            //colocamos la camtidad de-segun la profundidad
            //asemos un zoo de strig para que repita el guion  tantas veses como sea el valor de profundidad
            return new String('-', pProfundidad) + Nombre + "\n\r";
        }
    }
}
